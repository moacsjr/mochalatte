var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser')
var cors = require('cors')

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.raw({type: ["application/xml"]})); // for parsing application/xml
app.use(cors())

var mockfile = path.join(__dirname, '/config', 'mock.json')
//read the program args
process.argv.forEach(function (val, index, array) {
    if(val.startsWith('mockFile=')){
        mockfile = val.substr(9)
    }
});

var mocks = JSON.parse(fs.readFileSync(mockfile, 'utf8'));
console.info("Mocks found in ", mockfile)
mocks.forEach(mock => {
    console.info("Mapping request ",  mock.path)
    const handleRequest = (req, res) => {
        console.info("Handling request ",  mock.path)
        if(typeof mock.response === 'string'){
            res.send(mock.response)
        }else if(Array.isArray(mock.response)){
            var found = false
            for( var option of mock.response){
                console.debug("Test against ", option)
                if(option.matchBoddy){
                    var re = new RegExp(option.matchBoddy, "gm")                    
                    found = re.test(req.body.toString('utf8'))
                    if(found){
                        console.log("Matched request on boddy : ", req.body.toString('utf8'))
                        res.sendFile(path.join(__dirname, '/mocks' + mock.path, option.file))
                        return
                    }
                }
            }
            if(!found){
                console.log("Not found!")
                res.status(404).send("NOT FOUND").end();
            }
        }
    }    
    if(mock.method === 'post'){
        app.post(mock.path, handleRequest); 
    }else if(mock.method === 'get'){
        app.get(mock.path, handleRequest); 
    }    
});

app.get('/', function (req, res) {
    res.send(JSON.stringify(mocks));
});

app.get('/mochalatte/config', function (req, res) {
    res.type('json').send({success: true, data: mocks});
});
  
app.listen(3001, function () {
    console.log('Example app listening on port 3001!');
});